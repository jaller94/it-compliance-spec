=== A standard for IT compliance reports

Highly individualized questionnaires costs companies a lot of hours of labour, which can be measured using the wages of the involved employees. Furthermore, the process is often tedious for employees with little reward of generating new value for either company’s main business. Nonetheless, there is a strong need for protection of the employee’s data.

A standard for documenting compliance can help this by making the information easier to capture and exchange. To achieve a meaningful adoption the standard should be open with a permissive license to enable a variety of applications to read and modify the data. Applications may display and compare compliances of vendors or help to adopt best practices.

==== Stakeholders

Various groups will benefit from a unified standard for IT compliance reports. Here are important groups and their reasons to support the specification:

.Service and software vendors
* Make compliance more visible to customers
* Verify their own compliance by matching their reports to certification and governmental requirements
* Verify the compliance of used applications, contractors and services
* Reduce time spend filling our individually submitted questionnaires from customers
* Easily identify deficits and break them down into manageable tasks
* Streamline communication between Information Security staff, management and third parties like compliance firms

.Customers
* Better transparency about the services they use
* Instead of one-time questionnaires, a vendor can be expected to continuously publish their compliance
* Reduce time spend creating, reading and processing reports 
* Compliance can more easily be tracked over time by versioning received reports

.Certification agencies
* Publish standards for vendors to match their reports against
* Gather, share and digitally sign compliance reports
* Quickly identify deficits in a vendor’s service to focus support

.Governments
* Publish standards for vendors to match their reports against
* Make requirements in the law more accessible to vendors and customers
* Oversee vendor’s compliance with the law

.Compliance firms
* Streamline their services to support vendors becoming compliant
  * Services include: automated and manual advisory, device management software and compliance deep scanning

.All parties
* Gain insights about the adoption of certain best practices
* Reduce repetitive processes by easing initial communication
* Break down language barriers with translations
* Break down differences in cultural perception with precise compliance definitions

==== XML as the exchange format

To make the format easy to adopt, process and exchange it should be a text format. These have the benefit that their file content is both readable for machines and humans alike.

XML is a well-defined text format with many parsers in all mayor programming languages. It is preferable over the popular JSON format in cases where a lot of different implementations are expected to have different levels of support for the standard. This fits with the idea that various applications can make use of compliance data. A device management platform may read or write requirements for password complexity and screen lock configurations, while an application to identify deficits while read a wider variety of keys without interpreting any specific rules.

Schemas are another powerful feature of XML files which have been around for several years. These are combinable definitions of keys and value validations for XML files. XML Schema is the successor of a dozen schema formats for XML. It’s 1.0 definition was released in 2004, 10 years before the current version 1.1 has been released by the W3C authority. It’s successfully used for RSS newsfeeds, Podcast definitions and documents (see DOCX in Microsoft Office and ODT in LibreOffice).

This high availability of parsers and the long-term consistency, makes XML the best choice for exchanging reports which may be stored for decades out of legal reasons.
